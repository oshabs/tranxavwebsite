<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
    <link rel="stylesheet" href="{{asset('libraries/bootstrap/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('site.css')}}">
    <link rel="stylesheet" href="{{asset('fontawesome/css/all.css')}}">
    <link rel="stylesheet" href="{{asset('fontawesome/css/fontawesome.css')}}">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-custom">
        <a class="navbar-brand" href="#">Logo</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse maximum-wid" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
            </ul>
            <ul class="navbar-nav">

                <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">About Us</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">How it works</a>
                </li>
            </ul>
        </div>
    </nav>


    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="{{asset('images/first_banner.png')}}" alt="First slide">
                <div class="carousel-caption d-none d-md-block">
                    <div class="first-block">
                        <h5>YOUR VEHICLE IS </h5>
                        <h3>SAFE IN OUR HANDS</h3>
                        <a href="" class="btn btn-primary">Learn More ></a>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{asset('images/banner.png')}}" alt="Second slide">
                <div class="carousel-caption d-none d-md-block">
                    <h5>...</h5>
                    <p>...</p>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{asset('images/third_banner.png')}}" alt="Third slide">
                <div class="carousel-caption d-none d-md-block">
                    <h5>...</h5>
                    <p>...</p>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

    <div class="why-choose-us">
        <div class="contain mt-5">
            <div class="row">
                <div class="col-md-5 col-sm-12 col-xs-12 pt-3">
                    <h1>Why Choose Us?</h1>
                    <p class="choose-words">
                        Our most popular service is car repair. We know that sometimes you might be in an area where you
                        cant get a mechanic easily or you are too tired to take your car to the mechanic.
                        Let us do these transactions for you. With or services you dont need to stress your self any
                        longer. Just few clicks and all transactions are done.
                    </p>
                </div>
                <div class="col-md-7  col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="why">
                                <div class="row">
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        sdfwef
                                    </div>
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <h5>Flexibility</h5>
                                        <p>
                                            We give our customers the most flexible time.
                                        </p>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="why">
                                <div class="row">
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        sdfwef
                                    </div>
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <h5>Trust</h5>
                                        <p>
                                            Just first transaction will convince you.
                                        </p>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="why">
                                <div class="row">
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        sdfwef
                                    </div>
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <h5>Speedy Repair</h5>
                                        <p>
                                            Our Mechanic will repair your car as soon as possible.
                                        </p>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="why">
                                <div class="row">
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        sdfwef
                                    </div>
                                    <div class="col-md-8 col-sm-12 col-xs-12">
                                        <h5>Within Your Reach</h5>
                                        <p>
                                            We are closer to you than you think.
                                        </p>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="we-repair-these-vehicles">



        <div class="lines">
        </div>
        <div class="d-flex justify-content-center">
            <i class="fas fa-assistive-listening-systems"></i>

        </div>



        <div style="margin: 0 auto; max-width: 50%; backround: red;">
            <h2>WE REPAIR AND SERVICE THESE VEHICLES</h2>
            <p>Our Maitenance and repaires are high quality. We make sure your car is in good shape and in order
                before you carry it. We reapir and test it to our standard to make sure it si not below standard.
            </p>
        </div>

        <div class="we-repair-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="we-repair-inside">


                            <i class="fas fa-truck-moving"></i>

                            <h4>TRUCKS</h4>


                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="we-repair-inside">
                            <div class="">
                                <h3>
                                    <i class="fas fa-car"></i>
                                </h3>
                                <h4>LUXURY CARS</h4>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="we-repair-inside" style="border-right: 1px solid red;">
                            <div class="">
                                <h3>
                                    <i class="fas fa-truck-moving"></i>
                                </h3>
                                <h4>VANS</h4>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="contact-us">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-2">
                            <i class="fas fa-phone-alt"></i>
                        </div>
                        <div class="col-md-8">
                            <h6>24/7 SUPPORT</h6>
                            <p>08063574856</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-2">
                            <i class="fas fa-envelope"></i>
                        </div>
                        <div class="col-md-8">
                            <h6>Email Address</h6>
                            <p>info@tranxav.com</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-2">
                            <i class="fas fa-igloo"></i>
                        </div>
                        <div class="col-md-8">
                            <h6>Opening Hours</h6>
                            <p>Mon-Fri 8am to 6pm</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <footer>
        
    </footer>
    @yield('content')
    <script src="{{asset('libraries/jquery/jquery-2.2.4.min.js')}}"></script>
    <script src="{{asset('libraries/bootstrap/bootstrap.min.js')}}"></script>
</body>

</html>
